package it.epicode.be05.gamecards.model;

public class FrenchDeck extends Deck {

	public FrenchDeck() {
		// richiama il costruttore della propria classe base
		super();
		// inizializza il mazzo di carte
		for (var s = 0; s < 4; ++s)
			for (var v = 1; v < 14; ++v) {
				var value = v;
				var seed = FrenchCard.Seeds.values()[s];
				var fc = new FrenchCard(value, seed);
				cards.add(fc);
			}
	}
}
